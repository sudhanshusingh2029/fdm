import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

// The Download Manager
public class DownloadManager extends JFrame implements Observer
{
            //Add download text field
    private JTextField addTextField;
            //Download table's data model
    private DownloadsTableModel tableModel;
            //Table listing downloads
    private JTable table;
            //Buttons for managing selected downloads
    private JButton pauseButton, resumeButton;
    private JButton cancelButton, clearButton;
    
            // Currently selected download
    private Download selectedDownload;
            //Flag for whether or not table selection is being cleared
    private boolean clearing;
    
            // constructor for Downlaod Manager
    public DownloadManager()
    {
            //Set application title
        setTitle("Download Manager");
            //Set window size
        setSize(640,480);
            // Handle window closing events
        addWindowListener(new WindowAdapter(){
        public void windowClossing(WindowEvent e){
            actionExit();
        }
        });
            //set up file menu
        JMenuBar menuBar =new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
	JMenuItem fileExitMenuItem = new JMenuItem("Exit",KeyEvent.VK_X);      
	fileExitMenuItem.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                actionExit();
            }
        });
        fileMenu.add(fileExitMenuItem);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
        
            // set up add panel.
    JPanel addPanel= new JPanel();
    addTextField = new JTextField(30);
    addPanel.add(addTextField);
    JButton addButton = new JButton("Add Download");
    addButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            actionAdd();
        }
    });
    addPanel.add(addButton);
            //set up Downloads table
    tableModel = new DownloadsTableModel();
    table = new JTable (tableModel);
    table.getSelectionModel().addListSelectionListener(new 
    ListSelectionListener(){
        public void valueChanged(ListSelectionEvent e){
            tableSelectionChanged();
        }
    });
            //Allow only one row to be selected at a time
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            // set up ProgressBar as renderer for progress column
    ProgressRenderer renderer = new ProgressRenderer(0,100);
            //set progress text
    renderer.setStringPainted(true);
    table.setDefaultRenderer(JProgressBar.class,renderer);
            // set table's row height enough to fit JProgressBar
    table.setRowHeight(
        (int) renderer.getPreferredSize().getHeight());
            // set up downloads panel
    JPanel downloadsPanel = new JPanel();
    downloadsPanel.setBorder(
        BorderFactory.createTitledBorder("Downloads"));
    downloadsPanel.setLayout(new BorderLayout());
    downloadsPanel.add(new JScrollPane(table),BorderLayout.CENTER);
            //Set up buttons panel
    JPanel buttonsPanel =new JPanel();
    
    pauseButton= new JButton("Pause");
    pauseButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            actionPause();
        }
    });
    pauseButton.setEnabled(false);
    buttonsPanel.add(pauseButton);
    resumeButton=new JButton("Resume");
    resumeButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            actionResume();
        }
    });
    resumeButton.setEnabled(false);
    buttonsPanel.add(resumeButton);
    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            actionCancel();
        }
    });
    cancelButton.setEnabled(false);
    buttonsPanel.add(cancelButton);
    clearButton=new JButton("Clear");
    clearButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            actionClear();
        }
    });
    clearButton.setEnabled(false);
    buttonsPanel.add(clearButton);
            // Add panel to display
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(addPanel,BorderLayout.NORTH);
    getContentPane().add(downloadsPanel,BorderLayout.CENTER);
    getContentPane().add(buttonsPanel,BorderLayout.SOUTH);
    }       
            //EXIT this program
    private void actionExit()
    {
        System.exit(0);
    }
            // Add a new download
    private void actionAdd()
    {
        URL verifiedUrl=verifyUrl(addTextField.getText());
        if(verifiedUrl != null)
        {
            tableModel.addDownload(new Download(verifiedUrl));
            addTextField.setText(""); // reset add text field
        }
        else
        {
            JOptionPane.showMessageDialog(this,"Invalid Download URL","Error",JOptionPane.ERROR_MESSAGE);
        }
    }
            //VERIFY DOWNLOAD URL.
    private URL verifyUrl(String url)
    {
            //only allow HTTP URLAs
        if(!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("http://"))) { //change has been made here from http to https 23 october 2019 9:46 pm
	System.out.println("Hello in the https ");        
	return null;}
            //verify format of URL
        URL verifiedUrl=null;
        try
        {
            verifiedUrl=new URL(url);
        }
        catch(Exception e)
        {	
		System.out.println("Here in assigning a new url");
            return null;
        }
            //Make sure URL specifies a file
        if(verifiedUrl.getFile().length()<2){
	System.out.println("Hello error in file length.");
        return null;}
        
        return verifiedUrl;
    }
            //called when table row selection changes
    private void tableSelectionChanged()
    {
            /*Unregister from receiving notifications from the last selection download. */
        if(selectedDownload != null)
        selectedDownload.deleteObserver(DownloadManager.this);
            /*If not in the middle of clearing a download, set the selected download and registe
            to recieve notifications from it.*/
        if(!clearing && table.getSelectedRow()>-1)
        {
            selectedDownload=tableModel.getDownload(table.getSelectedRow());
            selectedDownload.addObserver(DownloadManager.this);
            updateButtons();
        }
    }
    
            //Pause the selected download
    private void actionPause()
    {
        selectedDownload.pause();
        updateButtons();
    }
            //Resume the selected download
    private void actionResume()
    {
        selectedDownload.resume();
        updateButtons();
    }
            // Cancel the selected download
    private void actionCancel()
    {
        selectedDownload.cancel();
        updateButtons();
    }
            //Clear the selected download
    private void actionClear()
    {
        clearing =true;
        tableModel.clearDownload(table.getSelectedRow());
        clearing =false;
        selectedDownload =null;
        updateButtons();
    }
    /*Update each button's state based off of the currently selected download's status */
    private void updateButtons()
    {
        if(selectedDownload != null)
        {
            int status =selectedDownload.getStatus();
            switch(status)
            {
                case Download.DOWNLOADING:
                    pauseButton.setEnabled(true);
                    resumeButton.setEnabled(false);
                    cancelButton.setEnabled(true);
                    clearButton.setEnabled(false);
                    break;
                case Download.PAUSED:
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(true);
                    cancelButton.setEnabled(true);
                    clearButton.setEnabled(false);
                    break;
                case Download.ERROR:
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(true);
                    cancelButton.setEnabled(false);
                    clearButton.setEnabled(true);
                    break;
                default: // COMPLETE OR CANCELLED
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(false);
                    cancelButton.setEnabled(false);
                    clearButton.setEnabled(true);
            }
        }
        else
        {
            //No download is selected in table.
            pauseButton.setEnabled(false);
            resumeButton.setEnabled(false);
            cancelButton.setEnabled(false);
            clearButton.setEnabled(false);
        }
    }
    /*Update is called when a downlod notifies its observers of any changes. */
    public void update(Observable o,Object arg)
    {
        //update buttons if the selected download has changed
        if(selectedDownload !=null && selectedDownload.equals(o))
            SwingUtilities.invokeLater(new Runnable(){
               public void run(){
                   updateButtons();
               } 
            });
    }
        // Run the Download Manager
    public static void main(String [] args)
    {
        SwingUtilities.invokeLater(new Runnable (){
            public void run(){
                DownloadManager manager =new DownloadManager();
                manager.setVisible(true);
            }
        });
    }
}

