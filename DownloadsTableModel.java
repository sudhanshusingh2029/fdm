import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

// This class manages the download table's data.
class DownloadsTableModel extends AbstractTableModel implements Observer
{
            // COLUMN NAMES 
    private static final String[] columnNames={"URL","Size","Progress","Status"};
            // CLASSES FOR COLUMN'S VALUES
    private static final Class[] columnClasses ={String.class,String.class,JProgressBar.class,String.class};
            // TABLE'S LIST OF DOWNLOADS
    private ArrayList<Download> downloadList=new ArrayList<Download>();
            // ADD NEW DOWN TO TABLE
    public void addDownload(Download download)
    {
            // Register to notified when the download changes
        download.addObserver(this);
        downloadList.add(download);
            // Fire table row insertion notification to table
        fireTableRowsInserted(getRowCount()-1,getRowCount()-1);
    }
            // GET A DOWNLOAD FOR  SPECIFIED ROW
    public Download getDownload(int row)
    {
        return downloadList.get(row);
    }
            // Remove a download from list
    public void clearDownload(int row)
    {
        downloadList.remove(row);
    
            // Fire table row deletion notification to table
    	fireTableRowsDeleted(row,row);
    }
            // Get table's column count
    public int getColumnCount()
    {
        return columnNames.length;
    }
            // Get a colulmn's name
    public String getColumnName(int col)
    {
        return columnNames[col];
    }
            // Get table's row count
    public int getRowCount()
    {
        return downloadList.size();
    }
            // Get a column's class
    public Class<?> getColumnClass(int col)
    {
        return columnClasses[col];
    }
            // Get value for specific row & column combination
    public Object getValueAt(int row ,int col)
    {
        Download download = downloadList.get(row);
        switch(col)
        {
            case 0: //url
                return download.getUrl();
            case 1: //size
                int size=download.getSize();
                return (size == -1)? "":Integer.toString(size);
            case 2: //progress
                return new Float (download.getProgress());
            case 3: //status
                return Download.STATUSES[download.getStatus()];
        }
        return "";
    }
    /* Update is called when a Download notifies its observers of any changes. */
    public void update(Observable o,Object arg)
    {
        int index=downloadList.indexOf(o);
                // Fire table rows update notification to table
        fireTableRowsUpdated(index,index);
    }    
}
