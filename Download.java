import java.io.*;
import java.net.*;
import java.util.*;

//This class downloads a file from a URL
class Download extends Observable implements Runnable
{
            //MAX SIZE OF DOWN BUFFER
    private static final int MAX_BUFFER_SIZE = 1024; 
            //STATUS NAMES
    public static final String STATUSES[]={"Downloading","Paused","Complete","Cancelled","Errror"};
            //STATUS CODES
    public static final int DOWNLOADING =0;
    public static final int PAUSED=1;
    public static final int COMPLETE=2;
    public static final int CANCELLED=3;
    public static final int ERROR=4;
            //DOWNLOAD URL
    private URL url; 
            //SIZE OF FILE TO BE DOWNLOADED
    private int size; 
            //No. of BYTES DOWNLOADED
    private int downloaded; 
            //CURRENT STATUS OF DOWNLOAD 
    private int status; 
    
    public Download(URL url)
    {
        this.url=url;
        size=-1;
        downloaded=0;
        status=DOWNLOADING;
            //BEGIN DOWNLOAD
        download(); 
    }
            // DOWN'S STRING
    public String getUrl() 
    {
        return url.toString();
    }
            // DOWN'S SIZE
    public int getSize()   
    {
        return size;
    }
            // DOWN'S PROGRESS
    public float getProgress() 
    {
        return ((float)downloaded/size)*100;
    }
            // DOWN'S STATUS
    public int getStatus() 
    {
        return status;
    }
            // PAUSE DOWN
    public void pause() 
    {
        status=PAUSED;
        stateChanged();
    }
            // RESUME DOWN
    public void resume() 
    {
        status = DOWNLOADING;
        stateChanged();
        download();
    }
            // CANCEL DOWN
    public void cancel() 
    {
        status = CANCELLED;
        stateChanged();
    }
            // ERROR DOWN
    private void error() 
    {
        status= ERROR;
        stateChanged();
    }
            // START / RESUME DOWN
    private void download() 
    {
        Thread thread=new Thread(this);
        thread.start();
    }
            // GET FILE NAME FROM URL
    private String getFileName(URL url) 
    {
        String fileName=url.getFile();
        return fileName.substring(fileName.lastIndexOf('/')+1);
    }
            // DOWNLOAD FILE 
    public void run() 
    {
        RandomAccessFile file=null;
        InputStream stream=null;
        try
        {
                        // OPEN CONNECTION TO URL
            HttpURLConnection connection=(HttpURLConnection)url.openConnection();
                        // SPECIFY PORTION TO BE DOWNLOADED
            connection.setRequestProperty("Range","bytes="+downloaded+"-");
                        // CONNECT TO SERVER
            connection.connect();
                        // RESPONSE CODE IS IN RANGE 200
            if(connection.getResponseCode()/100 != 2)
            {
		System.out.println("response codes ");
                error();
            }
                        // CONTENT LENGTH VALID
            int contentLength = connection.getContentLength();
            if(contentLength < 1)
            {
		System.out.println("contentlength ");
                error();
            }
                        // set size for down if not already set
            if(size == -1)
            {
                size = contentLength;
                stateChanged();
            }
                        // OPEN FILE AND SEEK END
            file = new RandomAccessFile(getFileName(url),"rw");
            file.seek(downloaded);
            
            stream= connection.getInputStream();
            while(status == DOWNLOADING)
            {
                byte buffer[];
                        // SIZE BUFFER acco... to HOW MUCH OF THE FILE IS LEFT TO DOWN
                if(size - downloaded > MAX_BUFFER_SIZE)
                {
                    buffer=new byte[MAX_BUFFER_SIZE];
                }
                else
                {
                    buffer = new byte[size - downloaded];
                }
                        // READ FROM SERVER INTO BUFFER
                int read = stream.read(buffer);
                if(read ==-1)
                break;
                
                file.write(buffer,0,read);
                downloaded+=read;
                stateChanged();
            }
                        // CHANGE STATUS bcs.. DOWNLOAD COMPLETE AT THIS POINT
            if(status== DOWNLOADING)
            {
                status=COMPLETE;
                stateChanged();
            }
            
        }
        catch(Exception e)
        {
            error();
        }
        finally
        {       // CLOSE FILE
            if(file != null) 
            {
                try
                {
                    file.close();
                }
                catch(Exception e){}
            }
                // CLOSE CONNECTION TO SERVER
            if(stream!=null) 
            {
                try
                {
                    stream.close();
                }
                catch(Exception e){}
            }
        }
    }
            // NOTIFY OBSERVERS i.e. THIS DOWNLOAD'S STATUS CHANGED
    private void stateChanged() 
    {
        setChanged();
        notifyObservers();
    }
} 
